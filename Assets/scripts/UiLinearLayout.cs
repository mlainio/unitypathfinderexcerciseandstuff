﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UiLinearLayout : MonoBehaviour {

    private List<GameObject> m_children = new List<GameObject>();
    public float MarginTop = 5f;
    public float MarginBottom = 5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Call ConfigureChildPositions after you're done adding
    /// </summary>
    /// <param name="child"></param>
    public void AddChild(GameObject child)
    {
        m_children.Add( child );
        child.transform.SetParent( transform, false );
        //ConfigureChildPositions();
    }

    public void ConfigureChildPositions()
    {
        float size = MarginTop;
        RectTransform t;
        for ( int i = 0; i < m_children.Count; i++ )
        {
            t = m_children[i].transform as RectTransform;

            t.anchoredPosition = new Vector2( t.anchoredPosition.x, -size );
            size += t.sizeDelta.y;
        }
        
        //Resize self
        t = (transform as RectTransform);
        t.sizeDelta = new Vector2( t.sizeDelta.x, size+MarginBottom );

    }
}
