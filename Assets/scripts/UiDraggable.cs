﻿using UnityEngine;
using System.Collections;
using System;

public class UiDraggable : MonoBehaviour
{
    public string CanvasName = "Canvas";
    public KeyCode DragKey = KeyCode.Mouse0;
    public RectTransform Draggable;
    private RectTransform canvas;

    private Vector2 m_originalPosition;
    private Vector2 m_originalCursorPosition;
    
    

    // Use this for initialization
    void Start()
    {
        if ( Draggable == null )
            Draggable = transform as RectTransform;

        canvas = GameObject.Find( CanvasName ).GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {

        // OnClick...
        //if ( Input.GetKeyDown( DragKey ) )
        //{
        //    StartDrag();
        //}
        //if ( Input.GetKey( DragKey ) )
        //{
        //    Drag();
        //}
        //if (Input.GetKeyUp(DragKey))
        //{
        //    ReleaseDrag();
        //}
    }

    private void ReleaseDrag()
    {
        ModeChange.Resume();
    }

    private void Drag()
    {
        Vector2 mousepos = UiUtil.GetRectPositionFromScreenPoint( canvas, Input.mousePosition );
        Debug.Log( "Cursor position: " + mousepos );
        Vector2 dragDelta = mousepos - m_originalCursorPosition;
        Draggable.anchoredPosition = m_originalPosition + dragDelta;
    }

    private void StartDrag()
    {
        m_originalPosition = Draggable.anchoredPosition;
        m_originalCursorPosition = UiUtil.GetRectPositionFromScreenPoint( canvas, Input.mousePosition );
        ModeChange.Pause();
    }
}
