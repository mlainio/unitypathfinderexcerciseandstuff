﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Graph;
using System;

[Obsolete]
public class LayeredSpriteGraph : MonoBehaviour
{
    public static Vector2 NORTH = new Vector2( 0, 1 );
    public static Vector2 SOUTH = new Vector2( 0, -1 );
    public static Vector2 EAST = new Vector2( 1, 0 );
    public static Vector2 WEST = new Vector2( -1, 0 );
    public Vector2[] directions = { NORTH, SOUTH, EAST, WEST };

    public Sprite BaseGridSprite;
    public Sprite FilledGridSprite;
    public Color32 ObstacleColor = Color.red;
    public Color32 PathColor = Color.blue;
    public Color32 FrontierColor = Color.green;
    public Color32 ExploredColor = Color.gray;
    public Color32 OriginColor = Color.white;
    public Color32 GoalColor = Color.yellow;
    public int Width;
    public int Height;
    private List<object> m_allNodes;
    private object m_goal;
    private object m_origin;
    private List<object> m_frontier;
    // Use this for initialization

    public void getNode(int x, int y)
    {

    }

    //void Start()
    //{
    //    m_allNodes = new List<object>();
    //    m_frontier = new List<object>();
               

    //    for ( int x = 0; x < Width; x++ )
    //    {
    //        for ( int y = 0; y < Height; y++ )
    //        {
    //            --m_allNodes.Add( new object( x, y, CreateNodeObject( x, y ) ) );
    //        }
    //    }
    //}

    //private List<object> getNeighbours( object node )
    //{
    //    List<object> neighbours = new List<object>();

    //    foreach ( Vector2 direction in directions )
    //    {
    //        object next = getNode( node.Position + direction );
    //        if ( next != null )
    //        {
    //            neighbours.Add( next );
    //        }
    //    }

    //    return neighbours;
    //}

    private GameObject CreateNodeObject( int x, int y )
    {
        GameObject nodeObject = new GameObject( "GridNode " + x + " " + y );
        nodeObject.transform.position = new Vector3( x, y, 0 );
        nodeObject.transform.parent = this.transform;
        nodeObject.AddComponent<SpriteRenderer>().sprite = BaseGridSprite;


        return nodeObject;
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if ( CoreData.Singleton.CurrentControlMode != ControlMode.Navigation && m_frontier.Count > 0 )
    //    {
    //        m_frontier.ForEach(node => node.OverlayObjects[1].SetActive(false));
    //    }
    //    else if (CoreData.Singleton.CurrentControlMode == ControlMode.Navigation)
    //    {
    //        m_frontier.ForEach( node => node.OverlayObjects[ 1 ].SetActive( true ) );
    //    }
        
    //    if ( Input.GetMouseButton( 0 ) )
    //    {
    //        Vector3 position = Camera.main.ScreenToWorldPoint( Input.mousePosition );
    //        switch ( CoreData.Singleton.CurrentControlMode )
    //        {
    //            case ControlMode.Paint:
    //                paintObstacle( position );
    //                break;
    //            case ControlMode.Navigation:
    //                setNavigationPoint( position );
    //                break;
    //        }


    //    }
    //    if ( Input.GetMouseButton( 1 ) )
    //    {
    //        Vector3 position = Camera.main.ScreenToWorldPoint( Input.mousePosition );
    //        switch ( CoreData.Singleton.CurrentControlMode )
    //        {
    //            case ControlMode.Paint:
    //                removePainted( position );
    //                break;
    //            case ControlMode.Navigation:
    //                if ( setOriginPoint( position ) )
    //                    resetFrontier();
    //                break;
    //        }

    //    }

    //}

    //private void clearFrontier()
    //{
    //    Debug.Log( "Clearing Frontier, size "+m_frontier.Count );
    //    foreach ( object node in m_frontier )
    //    {
    //        node.RemoveOverlaySprite( 1 );
    //    }
    //    m_frontier.Clear();
    //}
    //private void resetFrontier()
    //{
    //    clearFrontier();

    //    //foreach ( object node in getNeighbours( m_origin ) )
    //    //{
    //    //    setFrontier( node );
    //    //}
    //}

    //private void setFrontier( object node )
    //{        
    //    if ( node != null )
    //    {
    //        node.SetOverlaySprite( FilledGridSprite, FrontierColor, 1 );
    //        m_frontier.Add( node );
    //    }
    //}

    //private bool setOriginPoint( Vector3 position )
    //{
    //    object node = getNode( position );
    //    if ( node != null && node != m_origin )
    //    {
    //        if ( m_origin != null )
    //            removePainted( m_origin.PositionVector2 );

    //        node.SetOverlaySprite( FilledGridSprite, OriginColor );
    //        node.nodeCostModifier = 0;
    //        //node.CurrentOverlayType = object.OverlayType.Origin;
    //        m_origin = node;
    //        return true;
    //    }
        
        
    //    return false;
    //}

    //private void setNavigationPoint( Vector3 position )
    //{
    //    object node = getNode( position );
    //    if ( node != null && node != m_goal )
    //    {
    //        if ( m_goal != null )
    //            removePainted( m_goal.PositionVector2 );

    //        node.SetOverlaySprite( FilledGridSprite, GoalColor );
    //        node.nodeCostModifier = 0;
    //        //node.CurrentOverlayType = object.OverlayType.Goal;
    //        m_goal = node;
    //    }
    //}

    //private void removePainted( Vector3 position )
    //{
    //    object node = getNode( position );
    //    if ( node != null )
    //    {
    //        node.RemoveOverlaySprite();
    //        node.nodeCostModifier = 0;
    //        //node.CurrentOverlayType = object.OverlayType.None;
    //    }
    //}
    /// <summary>
    /// Transforms position coordinates to array index and fetches the node in question.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    private object getNode( Vector2 position )
    {
        int x = (int) position.x;
        int y = (int) position.y;

        if ( x < 0 || x >= Width || y < 0 || y >= Height )
            return null;

        int index = x * Height + y;
        return m_allNodes[ index ];
    }

    //private void paintObstacle( Vector3 position )
    //{
    //    SpriteOverlayNode2D node = getNode( position );
    //    if ( node != null )
    //    {
    //        node.SetOverlaySprite( FilledGridSprite, ObstacleColor );
    //        node.nodeCostModifier = 100;
    //        //node.CurrentOverlayType = SpriteNode.OverlayType.Obstacle;
    //    }
    //}
}
