﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public enum ControlMode
{
    None,
    Paint,
    Navigation,
}
public class ModeChange : MonoBehaviour
{
    private static ModeChange m_instance;
    private static ControlMode m_currentMode = ControlMode.None;
    private static ControlMode m_savedMode;

    public static ControlMode CurrentMode
    {
        get { return m_currentMode; }
    }

    void Awake()
    {
        m_instance = this;
    }
    // Use this for initialization
    void Start()
    {        
        ModeChange.m_currentMode = ControlMode.Paint;
        GetComponent<Text>().text = m_currentMode.ToString();
    }

    public static void SwitchMode()
    {
        int modeIndex = (int) m_currentMode;
        do
        {
            if ( ++modeIndex >= System.Enum.GetNames( typeof( ControlMode ) ).Length )
            {
                modeIndex = 0;
            }
            m_currentMode = (ControlMode) modeIndex;
            m_instance.GetComponent<Text>().text = m_currentMode.ToString();
        } while ( CurrentMode == ControlMode.None );
    }

    internal static void Resume()
    {
        m_currentMode = m_savedMode;
    }

    internal static void Pause()
    {
        m_savedMode = m_currentMode;
        m_currentMode = ControlMode.None;
    }
}
