﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HideShow : MonoBehaviour {
    public KeyCode Key = KeyCode.F1;
    public Behaviour Behaviour;
    private List<GameObject> m_children = new List<GameObject>();
    
	// Use this for initialization
	void Start () {

        if ( Behaviour != null )
        {
            for ( int i = 0; i < transform.childCount; i++ )
            {
                try
                {
                    Transform child = transform.GetChild(i);
                    m_children.Add( child.gameObject );
                }
                catch ( System.Exception e )
                {
                    Debug.LogError( e );
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(Key))
        {            
            try
            {
                Behaviour.enabled = !Behaviour.enabled;
                foreach (GameObject go in m_children)
                {
                    go.SetActive( Behaviour.enabled );
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError( "Whoops, can't enabled behaviour!! " + e );
            }
        }
	}
}
