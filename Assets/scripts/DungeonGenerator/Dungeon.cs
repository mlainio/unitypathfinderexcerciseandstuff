﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Dungeon
{
    public DungeonRoom[] RoomList = new DungeonRoom[0];
    public Square[,] SquareList;

    public int NumberOfRooms = 0;
    public int width;
    public int length;

    public Texture2D texture;

    public Dungeon(int sizeX, int sizeY, Texture2D newTexture)
    {
        SquareList = new Square[sizeX, sizeY];
        InitializeDungeon(sizeX, sizeY);
        texture = newTexture;
        width = sizeX;
        length = sizeY;
    }

    public Dungeon(int sizeX, int sizeY)
    {
        SquareList = new Square[sizeX, sizeY];
        InitializeDungeon(sizeX, sizeY);
        width = sizeX;
        length = sizeY;
    }

    private void InitializeDungeon(int sizeX, int sizeY)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                SquareList[x, y] = new Square(x, y);
            }
        }
    }

    public Square GetRight(Square sq)
    {
        if (0 < sq.posX + 1 && sq.posX + 1 < width && sq.posY > 0 && sq.posY + 1 < length)
            return SquareList[sq.posX + 1, sq.posY];
        else return null;
    }

    public Square GetLeft(Square sq)
    {
        if (0 < sq.posX - 1 && sq.posX - 1 < width && sq.posY > 0 && sq.posY < length)
            return SquareList[sq.posX - 1, sq.posY];
        else return null;
    }

    public Square GetAbove(Square sq)
    {
        if (0 < sq.posX && sq.posX < width && sq.posY + 1 > 0 && sq.posY + 1 < length)
            return SquareList[sq.posX, sq.posY + 1];
        else return null;
    }

    public Square GetBelow(Square sq)
    {
        if (0 < sq.posX && sq.posX < width && sq.posY - 1 > 0 && sq.posY - 1 < length)
            return SquareList[sq.posX, sq.posY - 1];
        else return null;
    }

    /// <summary>
    /// Returns 4 squares; above, left, right and below the given square.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public Square[] GetAdjacentSquares(int x, int y)
    {
        return new Square[] { SquareList[x, (y - 1)], SquareList[(x - 1), (y)], SquareList[x + 1, y], SquareList[x, y + 1] };
    }
}

public class DungeonRoom
{
    public List<Square> Squares;

    public DungeonRoom(List<Square> SquareList)
    {
        Squares = SquareList;
    }
}

public class Square
{
    public int posX;
    public int posY;

    private Color m_color;

    public Color Colour
    {
        get { return m_color; }
        set { m_color = value; }
    }

    public Square(int x, int y, Color colour)
    {
        posX = x;
        posY = y;
        Colour = colour;
    }

    public Square(int x, int y)
    {
        posX = x;
        posY = y;
    }
}
