﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Graph;

public class Generator : MonoBehaviour
{

    public bool GenerateWithMouse = false;
    public bool GenerateAtStart = false;
    public bool UseTexture2D = false;

    public int[] DungeonSize = { 40, 40 };


    public GUITexture GUIComponent;
    public Texture2D texture;

    public GraphPainter painter;
    public Paint floor = new Paint();
    public Paint wall = new Paint();

    List<int[,]> ProcessedCoordinates = new List<int[,]>();
    List<DungeonRoom> Rooms = new List<DungeonRoom>();

    int m_roomSize;
    int m_wallSize;

    public int ChanceForWall = 45;
    public int SmoothingIterations = 5;
    public bool EnforceRoomSize = true;
    public int MinRoomSize = 20;
    public bool EnforceWallSize = true;
    public int MinWallSize = 20;

    public Color WallColor = Color.white;
    public Color FloorColor = Color.blue;

    public Dungeon dungeon;

    // Use this for initialization
    void Start()
    {
        GUIComponent = GetComponent<GUITexture>();
        painter = GetComponent<GraphPainter>();

        DungeonSize[0] = painter.Height;
        DungeonSize[1] = painter.Width;

        if (GenerateAtStart)
        { GenerateDungeon(); }
        floor = painter.GetPaintByOverlay(ObjectLayer.Layer.Floor);
        wall = painter.GetPaintByOverlay(ObjectLayer.Layer.Wall);

    }

    // Update is called once per frame
    void Update()
    {
        if (GenerateWithMouse && Input.GetMouseButtonDown(0))
        {
            GenerateDungeon();
        }
    }

    public void ToggleUseTexture2D(bool setting)
    {
        UseTexture2D = setting;
    }

    public void GenerateDungeonButton()
    {
        GenerateDungeon();
    }

    public void SetSquaresByChance()
    {
        int wallCount = 0;
        int floorCount = 0;
        System.Random rng = new System.Random();
        for (int x = 0; x < DungeonSize[0]; x++)
        {
            for (int y = 0; y < DungeonSize[1]; y++)
            {
                if (rng.Next(100) >= ChanceForWall)
                {
                    SetFloor(x, y);
                    floorCount++;
                }
                else
                {
                    SetWall(x, y);
                    wallCount++;
                }
            }
        }
        Debug.Log("Created " + floorCount + " floorSquares and " + wallCount + " 'walls'.");
    }

    public void SetWallChance(string chance)
    {
        ChanceForWall = Int32.Parse(chance);
    }

    public void SetSquare(int x, int y, Color color)
    {
        texture.SetPixel(x, y, color);
        dungeon.SquareList[x, y].Colour = color;
    }

    public void SetWall(int x, int y)
    {
        SetSquare(x, y, WallColor);
    }

    public void SetFloor(int x, int y)
    {
        SetSquare(x, y, FloorColor);
    }

    public void RepositionGUI()
    {
        GUIComponent.pixelInset = new Rect(1180, 652, -830, -360);

    }

    /// <summary>
    /// Runs SetSquaresByChance(), SmoothRegions() times the set amount and ClearSmallRegions() according to EnforceRoomSize and EnforceWallSize.
    /// Prints the Texture2D or applies paint to GraphPainter according to UseTexture2D -boolean.
    /// </summary>
    public void GenerateDungeon()
    {
        DungeonSize[0] = painter.Height;
        DungeonSize[1] = painter.Width;
        CreateNewTexture();
        Rooms.Clear();
        painter.ClearAllPaint();
        RepositionGUI();

        if (UseTexture2D)
        {
            GUIComponent.enabled = true;
            dungeon = new Dungeon(DungeonSize[0], DungeonSize[1]);
            Debug.Log("Initialized a " + DungeonSize[0] + " * " + DungeonSize[1] + " map.");
            SetSquaresByChance();
            string smoothingText = "Smoothing changed ";
            for (int i = 0; i < SmoothingIterations; i++)
                smoothingText = smoothingText + SmoothRegions() + ", ";
            Debug.Log(smoothingText.Remove(smoothingText.Length - 2) + " squares per iteration.");

            if (EnforceRoomSize)
                ClearSmallRegions(FloorColor);
            if (EnforceWallSize)
                ClearSmallRegions(WallColor);
            texture.Apply();
        }
        else //Use GraphPainter
        {
            GUIComponent.enabled = false;
            dungeon = new Dungeon(DungeonSize[0], DungeonSize[1]);
            Debug.Log("Initialized a " + DungeonSize[0] + " * " + DungeonSize[1] + " map.");
            SetSquaresByChance();
            string smoothingText = "Smoothing changed ";
            for (int i = 0; i < SmoothingIterations; i++)
                smoothingText = smoothingText + SmoothRegions() + ", ";
            Debug.Log(smoothingText.Remove(smoothingText.Length - 2) + " squares per iteration.");

            if (EnforceRoomSize)
                ClearSmallRegions(FloorColor);
            if (EnforceWallSize)
                ClearSmallRegions(WallColor);

            ApplyGraph();
        }
        MapRooms();

        Debug.Log("Mapped " + Rooms.Count + " room regions.");

    }

    public void ClearTinyWalls()
    {
        ClearSmallRegions(WallColor);
        ApplyGraph();
        texture.Apply();
    }

    public void ClearTinyRooms()
    {
        ClearSmallRegions(FloorColor);
        ApplyGraph();
        texture.Apply();
    }

    /// <summary>
    /// Applies paint to all GraphPainter's nodes according to each individual Square's color
    /// </summary>
    public void ApplyGraph()
    {
        painter.ClearAllPaint();
        for (int x = 0; x < DungeonSize[0]; x++)
        {
            for (int y = 0; y < DungeonSize[1]; y++)
            {
                if (dungeon.SquareList[x, y].Colour == FloorColor)
                {
                    painter.Paint(new Point2(x, y), floor);
                }
                else
                {
                    painter.Paint(new Point2(x, y), wall);
                }
            }
        }
    }

    public int SmoothRegions()
    {
        int count = 0;
        for (int x = 0; x < DungeonSize[0]; x++)
        {
            for (int y = 0; y < DungeonSize[1]; y++)
            {
                int neighbourWalls = GetSurroundingWallCount(x, y);
                if (neighbourWalls > 4 && IsFloor(dungeon.SquareList[x, y].Colour))
                {
                    SetWall(x, y);
                    count++;
                }
                if (neighbourWalls < 4 && IsWall(dungeon.SquareList[x, y].Colour))
                {
                    SetFloor(x, y);
                    count++;
                }
            }
        }
        return count;
    }

    public void SetSmoothingIterations(string num)
    {
        SmoothingIterations = Int32.Parse(num);
    }

    public void SetMinRoomSize(string size)
    {
        MinRoomSize = Int32.Parse(size);
    }

    public void SetMinWallSize(string size)
    {
        MinWallSize = Int32.Parse(size);
    }

    public void ClearSmallRegions(Color color)
    {
        int changeCount = 0;
        for (int x = 0; x < DungeonSize[0]; x++)
        {
            for (int y = 0; y < DungeonSize[1]; y++)
            {
                if (dungeon.SquareList[x, y].Colour == color)
                {
                    if (IsFloor(color) && TooSmallRegion(dungeon.SquareList[x, y]))
                    {
                        SetWall(x, y);
                        changeCount++;
                    }
                    if (IsWall(color) && TooSmallRegion(dungeon.SquareList[x, y]))
                    {
                        SetFloor(x, y);
                        changeCount++;
                    }
                }
            }
        }
        Debug.Log("Changed " + changeCount + " squares because of minimum region size.");
    }

    public bool TooSmallRegion(Square sq)
    {
        if (IsFloor(sq.Colour) && GetRegion(sq).Count < MinRoomSize)
            return true;
        else if (IsWall(sq.Colour))
        {
            if (GetRegion(sq).Count < MinWallSize)
                return true;
        }

        return false;
    }

    public bool IsWall(Color color)
    {
        return (color == WallColor);
    }

    public bool IsFloor(Color color)
    {
        return (color == FloorColor);
    }

    /// <summary>
    /// Algorithm for detecting and returning a list of same colored Squares attached to the startSquare
    /// </summary>
    /// <param name="startSquare"></param>
    /// <returns></returns>
    public List<Square> GetRegion(Square startSquare)
    {
        List<Square> squaresFound = new List<Square>();
        int[,] flags = new int[DungeonSize[0], DungeonSize[1]];

        Queue<Square> queue = new Queue<Square>();
        queue.Enqueue(startSquare);
        flags[startSquare.posX, startSquare.posY] = 1;

        while (queue.Count > 0)
        {
            Square sq = queue.Dequeue();
            squaresFound.Add(sq);

            for (int x = sq.posX - 1; x <= sq.posX + 1; x++)
            {
                for (int y = sq.posY - 1; y <= sq.posY + 1; y++)
                {
                    if (IsInDungeonRange(x, y) && (x == sq.posX || y == sq.posY))
                    {
                        if (flags[x, y] == 0 && dungeon.SquareList[x, y].Colour == startSquare.Colour)
                        {
                            flags[x, y] = 1;
                            queue.Enqueue(dungeon.SquareList[x, y]); ;
                        }
                    }
                }
            }
        }
        return squaresFound;
    }

    public void MapRooms()
    {
        for (int x = 0; x < DungeonSize[0]; x++)
        {
            for (int y = 0; y < DungeonSize[1]; y++)
            {
                if (IsFloor(dungeon.SquareList[x, y].Colour) && !SquareExistsInMappedRegions(dungeon.SquareList[x, y]))
                {
                    Rooms.Add(new DungeonRoom(GetRegion(dungeon.SquareList[x, y])));
                }
            }
        }
    }


    public bool SquareExistsInMappedRegions(Square sq)
    {
        bool fact = false;

        for (int i = 0; i < Rooms.Count; i++)
        {
            for (int n = 0; n < Rooms[i].Squares.Count; n++)
            {
                if (Rooms[i].Squares[n].posX == sq.posX && Rooms[i].Squares[n].posY == sq.posY)
                    fact = true;
            }
        }
        return fact;
    }

    public int GetSurroundingWallCount(int x, int y)
    {
        int count = 0;

        for (int adjX = x - 1; adjX <= x + 1; adjX++)
        {
            for (int adjY = y - 1; adjY <= y + 1; adjY++)
            {
                if (IsInDungeonRange(adjX, adjY))
                {
                    if (IsWall(dungeon.SquareList[adjX, adjY].Colour))
                    {
                        count++;
                    }
                }
                else
                    count++;
            }
        }

        return count;
    }

    public bool IsInDungeonRange(int x, int y)
    {
        return (x >= 0 && x < dungeon.width && y >= 0 && y < dungeon.length);
    }

    public void CreateNewTexture()
    {
        texture = new Texture2D(DungeonSize[0], DungeonSize[1]);
        GUIComponent.texture = texture;
    }

    public int GetRandom(int min, int max)
    {
        return new System.Random().Next(min, max);
    }

}