﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiOnPaintSelected : MonoBehaviour {

    private Paint m_mypaint;
    public Image PaintColor;
    public Text PaintName;
    private Image m_background;
    private bool m_primary = false;
    private bool m_alt = false;
    public Color AltColor;
    public Color PrimaryColor;

    void Awake()
    {
        m_background = GetComponent<Image>();
        PaintColor = transform.FindChild("ColorBox").GetComponent<Image>();
        PaintName = transform.FindChild("OverlayText").GetComponent<Text>();
    }

    void Start()
    {
        m_background.enabled = false;
    }

	public void OnPaintSelected(Paint paint)
    {
        if (paint == m_mypaint)
        {
            m_background.color = PrimaryColor;
            m_background.enabled = true;
            m_primary = true;
        }
        else
        {
            m_background.enabled = false;
            m_primary = false;

        }
    }

    public void OnAltPaintSelected(Paint paint)
    {
        if ( paint == m_mypaint )
        {
            m_background.color = AltColor;
            m_background.enabled = true;
            m_alt = true;
        }
        else
        {
            if ( !m_primary )
                m_background.enabled = false;
            m_alt = false;
        }
    }

    public void OnPaintAssigned( Paint paint )
    {
        m_mypaint = paint;
        Color c = paint.Color;
        c.a = 1f;
        PaintColor.color = c;

        PaintName.text = paint.Name;
    }
}
