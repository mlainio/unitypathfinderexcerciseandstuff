﻿using UnityEngine;
using System.Collections;

public class UiUtil {

    /// <summary>
    /// Not implemented
    /// </summary>
    /// <param name="transform"></param>
    /// <returns></returns>
    public static Vector2 GetRectTransformWorldPosition( RectTransform transform )
    {
        return Vector2.zero;
    }

    public static Vector2 GetRectPositionFromScreenPoint(RectTransform transform, Vector2 screenPoint, Camera camera = null )
    {
        Vector2 pos;
        if ( camera == null )
            camera = Camera.main;

        if ( !RectTransformUtility.ScreenPointToLocalPointInRectangle( transform, screenPoint, camera, out pos ) )
            Debug.Log( "ScreenPointToLocalPointInRectangle Failed" );

        return pos;
    }
}
