﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Graph;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class Paint
{
    public Paint()
    {

    }
    public Color Color;
    public string Name
    {
        get
        {
            return Overlay.ToString();
        }
    }
    public ObjectLayer.Layer Overlay;
    public ControlMode SelectableInModes = ControlMode.Paint;
    public override string ToString()
    {
        string s = "Paint: "+Name+", Overlay: "+Overlay.ToString()+", Color: "+Color.ToString();
        return base.ToString() + ": " + s;
    }
}

public class GraphPainter : MonoBehaviour
{

    private LayeredSpriteGraph2D m_graph;
    public int Height;
    public int Width;
    public Paint[] Paints = new Paint[ObjectLayer.TypeCount];
    private int m_selectedPaint = 0;

    public KeyCode PaintButton = KeyCode.Mouse0;
    public KeyCode AltPaintButton = KeyCode.Mouse1;
    private int m_selectedAltPaint;
    private System.Collections.Generic.Dictionary<ObjectLayer.Layer, LayeredSpriteNode2D> m_singletonNodes = new System.Collections.Generic.Dictionary<ObjectLayer.Layer, LayeredSpriteNode2D>();
    private Paint m_frontierPaint;
    private PathFinding.Pathfinder m_pathfinder;
    private PathfinderMonitor m_monitor;
    private bool ShowIterations = true;

    public void SetShowIterations(bool show)
    {
        Debug.Log( "Show Iterations set to " + show );
        ShowIterations = show;
    }

    public void SetHeight(string height)
    {
        Debug.Log( "Reading height set to " + height );
        int h = Height;
        Int32.TryParse( height, out h );
        if ( h != Height )
        {
            Height = h;
            Debug.Log( "Height set to " + Height );
        }
    }
    public void SetWidth(string width)
    {
        Debug.Log( "Reading width set to " + width );
        int w = Width;
        Int32.TryParse( width, out w );
        if (w != Width)
        {
            Width = w;
            Debug.Log( "Width set to " + Width);
        }
    }

    public Paint FrontierPaint
    {
        get
        {
            if ( m_frontierPaint == null )
            {
                Debug.Log( "FrontierPaint reference is null, finding it..." );
                foreach ( Paint p in Paints )
                {
                    if ( p.Overlay == ObjectLayer.Layer.Frontier )
                    {
                        Debug.Log( "Paint found: " + p.Color + " Name: " + p.Name );
                        m_frontierPaint = p;
                        break;
                    }
                }
            }
            return m_frontierPaint;
        }
    }

    public Point2 NodePointFromMousePosition
    {

        get
        {
            int x=0, y=0;
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return new Point2( (int) pos.x, (int) pos.y );
        }
    }

    public void NextPaint()
    {
        do
        {
            m_selectedPaint++;

        } while ( SelectedPaint.SelectableInModes != ModeChange.CurrentMode );

        broadcastPaintSelected( SelectedPaint );
    }

    private void broadcastPaintSelected( Paint paint )
    {
        GameObject canvas = GameObject.Find( "Canvas" );
        if ( canvas != null )
            canvas.BroadcastMessage( "OnPaintSelected", paint, SendMessageOptions.DontRequireReceiver );
    }

    private void broadcastAltPaintSelected( Paint paint )
    {
        GameObject canvas = GameObject.Find( "Canvas" );
        if ( canvas != null )
            canvas.BroadcastMessage( "OnAltPaintSelected", paint, SendMessageOptions.DontRequireReceiver );
    }

    public Paint SelectedPaint
    {
        get
        {
            return GetPaintByIndex( m_selectedPaint );
        }
    }

    public Paint SelectedAltPaint
    {
        get
        {
            return GetPaintByIndex( m_selectedAltPaint );
        }
    }

    public Node2D Origin
    {
        get
        {
            LayeredSpriteNode2D node;
            m_singletonNodes.TryGetValue( ObjectLayer.Layer.Origin, out node );
            return node;
        }
    }
    public Node2D Goal
    {
        get
        {
            LayeredSpriteNode2D node;
            m_singletonNodes.TryGetValue( ObjectLayer.Layer.Goal, out node );
            return node;
        }
    }

    /// <summary>
    /// find paint index by overlay. returns first instance found.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>paint index or -1 if not found</returns>
    private int GetPaintIndexByOverlay( ObjectLayer.Layer type )
    {
        int i;
        for ( i = 0; i < Paints.Length; i++ )
        {
            if ( Paints[ i ].Overlay == type )
            {
                return i;
            }
        }
        return -1;
    }

    private Paint GetPaintByIndex( int index )
    {
        if ( index >= 0 )
        {
            return Paints[ index % Paints.Length ];
        }
        else
        {
            return Paints[ Paints.Length - ( index % Paints.Length ) ];
        }
    }

    /// <summary>
    /// Find paint by overlay. returns first instance matchin the criteria
    /// </summary>
    /// <param name="layer"></param>
    /// <returns>Paint if found or null if not</returns>
    public Paint GetPaintByOverlay( ObjectLayer.Layer layer )
    {
        foreach ( Paint p in Paints )
        {
            if ( p.Overlay == layer )
                return p;
        }

        return null;
    }

    private void createAlgorithmPanel()
    {
        UiLinearLayout algoPanel = GameObject.Find("AlgorithmPanel").GetComponent<UiLinearLayout>();
        GameObject go = Instantiate( Resources.Load<GameObject>("prefabs/DropdownBox" ) );
        algoPanel.AddChild(go);
        UnityEngine.UI.Dropdown dropdown = go.GetComponent<UnityEngine.UI.Dropdown>();
        List<Dropdown.OptionData> opts = new List<Dropdown.OptionData>();
        foreach (PathFinding.SearchAlgorithm a in Enum.GetValues(typeof(PathFinding.SearchAlgorithm)) )
        {
            Dropdown.OptionData item = new Dropdown.OptionData();
            item.text = a.ToString();
            opts.Add( item );
        }
        dropdown.AddOptions( opts );
        dropdown.onValueChanged.AddListener( OnAlgorithmChange);
    }

    public void OnAlgorithmChange(int value)
    {
        Debug.Log( "OnAlgorithmChange to " + value );
        try {
            PathFinding.SearchAlgorithm a = (PathFinding.SearchAlgorithm) Enum.GetValues( typeof( PathFinding.SearchAlgorithm ) ).GetValue( value );
            Debug.Log( "Enum equivalent would be: " + a );
            m_pathfinder.SearchAlgorithm = a;
        } catch (Exception e)
        {
            Debug.LogException( e );
        }
    }

    private void createPaintPanel()
    {
        UiLinearLayout paintPanel = GameObject.Find("PaintPanel").GetComponent<UiLinearLayout>();
        for ( int i = 0; i < Paints.Length; i++ )
        {
            if ( Paints[ i ] != null && Paints[ i ].Color == Color.clear )
            {
                Paints[ i ].Color = UnityEngine.Random.ColorHSV( 0f, 1f, 0f, 1f, 0f, 1f, 1f, 1f );
            }
            if ( paintPanel != null )
            {
                GameObject go = Instantiate( Resources.Load<GameObject>( "prefabs/ImageTextBox" ));
                go.GetComponent<UiOnPaintSelected>().OnPaintAssigned( Paints[ i ] );
                paintPanel.AddChild( go );
            }

        }
        paintPanel.ConfigureChildPositions();
        broadcastPaintSelected( SelectedPaint );
    }

    // Use this for initialization
    void Start()
    {
        createPaintPanel();
        createAlgorithmPanel();

        createGraph();
        //make sure the newly added ui objects are properly started before continuing.
        Invoke( "initPaint", 0.1f );
        m_monitor = new PathfinderMonitor( this );
        m_pathfinder = new PathFinding.Pathfinder( m_graph, m_monitor );

    }

    private void createGraph()
    {
        //if (m_graph != null)
        m_graph = new LayeredSpriteGraph2D( this.gameObject, Height, Width );
        m_graph.PopulateIterative();
    }

    public void RecreateGraph()
    {
        m_graph.deleteAllNodeObjects();
        Invoke("createGraph",0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        /*
        *   Input Handling for UI
        */

        if ( Input.GetKeyDown( KeyCode.Tab ) )
        {
            ModeChange.SwitchMode();
            initPaint();
        }

        if ( Input.GetKeyDown( KeyCode.Alpha1 ))
        {
            NextPaint();
        }

        if ( ModeChange.CurrentMode == ControlMode.Paint )
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartCoroutine( FillWithPaint( SelectedPaint ) );
            }
            if ( Input.GetKey( PaintButton ) )
            {
                //Debug.Log( "PaintPrimary!" );
                Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (m_graph.withinBounds(new Point2((int)p.x, (int)p.y)))
                    Paint( NodePointFromMousePosition, SelectedPaint );
            }

            if ( Input.GetKey( AltPaintButton ) )
            {
                //Debug.Log( "PaintAlt!" );
                //Paint( NodePointFromMousePosition, SelectedAltPaint );
                ( m_graph.GetNode( NodePointFromMousePosition ) as LayeredSpriteNode2D ).RemoveOverlaySprite( SelectedPaint.Overlay );
            }
        }
        else if ( ModeChange.CurrentMode == ControlMode.Navigation )
        {
            //if ( Input.GetKeyDown( KeyCode.Space ) )
            //{
            //    StartPathFinderRoutine();
            //}

            if ( Input.GetKeyDown( PaintButton ) )
            {
                Debug.Log( "Paint Origin " + SelectedPaint.ToString() );
                PaintSingletonNode( SelectedPaint, NodePointFromMousePosition );
            }

            if ( Input.GetKeyDown( AltPaintButton ) )
            {
                ( m_graph.GetNode( NodePointFromMousePosition ) as LayeredSpriteNode2D ).RemoveOverlaySprite( ObjectLayer.Layer.Goal );
                ( m_graph.GetNode( NodePointFromMousePosition ) as LayeredSpriteNode2D ).RemoveOverlaySprite( ObjectLayer.Layer.Origin );
            }
        }

    }

    public void StartPathFinderRoutine()
    {
        StartCoroutine( StartPathFinder() );
    }

    private IEnumerator StartPathFinder()
    {
        m_monitor.SearchActive = true;
        ClearPaint( m_monitor.LastPath, ObjectLayer.Layer.Path );
        do
        {
            m_pathfinder.Search( Origin, Goal );
            if (ShowIterations)
                yield return null;
        } while ( m_monitor.SearchActive );
        yield return null;
    }

    private IEnumerator FillWithPaint( Paint selectedPaint )
    {
        for ( int x = 0; x < Width; x++ )
        {
            for ( int y = 0; y < Height; y++ )
            {
                Paint( new Point2( x, y ), SelectedPaint );
                //yield return null;
            }
        }
        yield return null;
    }

    /// <summary>
    /// Paint a singleton node based on overlay. Only one such node can exist, the former is cleared
    /// </summary>
    /// <param name="node"></param>
    /// <param name="paint"></param>
    /// <param name="newPosition"></param>
    public void PaintSingletonNode( Paint paint, Point2 newPosition )
    {
        LayeredSpriteNode2D node= m_graph.GetNode( newPosition ) as LayeredSpriteNode2D;
        if ( node == null )
            return;
        if ( m_singletonNodes.ContainsKey( paint.Overlay ) )
        {
            if ( m_singletonNodes[paint.Overlay] != null )
                m_singletonNodes[ paint.Overlay ].RemoveOverlaySprite( paint.Overlay );

            m_singletonNodes[ paint.Overlay ] = node;
        }
        else
        {
            m_singletonNodes.Add( paint.Overlay, m_graph.GetNode( newPosition ) as LayeredSpriteNode2D );
        }

        m_singletonNodes[ paint.Overlay ].SetOverlaySprite( m_graph.BaseNodeSprite, paint.Color, paint.Overlay );
    }

    /// <summary>
    /// Paint a node at position with designated paint
    /// </summary>
    /// <param name="nodePosition"></param>
    /// <param name="paint"></param>
    public void Paint( Point2 nodePosition, Paint paint )
    {
        LayeredSpriteNode2D node = m_graph.GetNode(nodePosition) as LayeredSpriteNode2D;
        Paint( node, paint );
    }

    /// <summary>
    /// paint a specific node with specifi paint
    /// </summary>
    /// <param name="node"></param>
    /// <param name="paint"></param>
    public void Paint( Node2D node, Paint paint)
    {
        if ( node != null )
        {
            LayeredSpriteNode2D spriteNode = node as LayeredSpriteNode2D;
            spriteNode.SetOverlaySprite( m_graph.BaseNodeSprite, paint.Color, paint.Overlay );
            //aah stupid way to do this :(
            if ( paint.Overlay == ObjectLayer.Layer.PassableObstacle )
                spriteNode.Cost = System.Math.Max(spriteNode.Cost, 3);
        }
    }

    /// <summary>
    /// Refresh selected paint to the ui
    /// </summary>
    private void initPaint()
    {
        if ( ModeChange.CurrentMode == ControlMode.Paint )
        {
            m_selectedPaint = GetPaintIndexByOverlay( ObjectLayer.Layer.Wall );
            broadcastPaintSelected( SelectedPaint );
        }
        else if ( ModeChange.CurrentMode == ControlMode.Navigation )
        {
            m_selectedPaint = GetPaintIndexByOverlay( ObjectLayer.Layer.Origin );
            broadcastPaintSelected( SelectedPaint );
        }
    }

    /// <summary>
    /// Clears all paint from designated layers from designated nodes
    /// </summary>
    /// <param name="nodes"></param>
    /// <param name="layer"></param>
    private void ClearPaint( Node2D[] nodes, ObjectLayer.Layer layer )
    {
        if ( nodes != null )
        {
            foreach ( Node2D node in nodes )
            {
                try
                {
                    LayeredSpriteNode2D n = node as LayeredSpriteNode2D;
                    n.RemoveOverlaySprite( layer );
                    //Debug.Log( "Removing layer: " + layer + " from node " + n );
                }
                catch ( System.Exception e )
                {
                    Debug.LogException( e );
                }
            }
        }
    }

    public void ClearPaint(Point2[] points, ObjectLayer.Layer layer)
    {
        for ( int i = 0; i < points.Length; i++ )
        {
            
            LayeredSpriteNode2D node = m_graph.GetNode(points[i]) as LayeredSpriteNode2D;            
            node.RemoveOverlaySprite( layer );
            if ( m_singletonNodes.ContainsValue( node ) && node.GetOverlay( layer ) != null )
                m_singletonNodes.Remove( layer );
        }
    }
    public void ClearLayer(ObjectLayer.Layer layer)
    {
        if ( m_singletonNodes.ContainsKey( layer ) )
            m_singletonNodes.Remove( layer );
        m_graph.ClearOverlay( layer );
    }
    public void ClearAllPaint()
    {
        m_singletonNodes.Clear();
        m_graph.ClearEverything();
    }

    private void PaintNodes( Node2D[] nodes, ObjectLayer.Layer layer )
    {
        Paint paint = GetPaintByOverlay(layer);
        if ( nodes != null && nodes.Length > 0 )
            foreach ( Node2D node in nodes )
            {
                Paint( node, paint );
            }
        else
            Debug.Log( "Nothing to paint" );
    }

    /// <summary>
    /// Contains callback methods between pathfinder and the GraphPainter (ui)
    /// </summary>
    public class PathfinderMonitor : PathFinding.Pathfinder.ISearchCallback
    {
        private GraphPainter m_parent;
        private Node2D[] m_frontier;
        private bool m_searchActive = false;
        private Node2D[] m_lastPath;

        public bool SearchActive
        {
            get
            {
                return m_searchActive;
            }
            set
            {
                m_searchActive = value;
            }
        }

        public Node2D[] LastPath { get { return m_lastPath; } private set { m_lastPath = value; } }

        public PathfinderMonitor( GraphPainter parent )
        {
            m_parent = parent;
        }
        public void Done( Node2D[] path)
        {
            m_searchActive = false;
            LastPath = path;
            m_parent.PaintNodes( LastPath, ObjectLayer.Layer.Path );
        }

        public void UpdateFrontier( Node2D[] frontier )
        {
            m_parent.ClearPaint( m_frontier, ObjectLayer.Layer.Frontier );
            m_frontier = frontier;
            foreach ( Node2D node in m_frontier )
            {
                if (node != null)
                    m_parent.Paint( node.Position, m_parent.FrontierPaint );
            }
        }

        public void UpdateVisited( Node2D[] visited )
        {
            Debug.LogError( "Method not implemented!" );
        }

        public void ClearFrontier( Node2D[] frontier )
        {
            m_parent.ClearPaint( frontier, ObjectLayer.Layer.Frontier );
        }
    }

}
