﻿using UnityEngine;
using System.Collections;

public class CoreData : MonoBehaviour {

    public static CoreData Singleton;
    public ControlMode CurrentControlMode
    {
        get { return ModeChange.CurrentMode; }
    }

    private ModeChange m_modeObject;

	// Use this for initialization
	void Start () {
        Singleton = this;
        m_modeObject = GameObject.Find( "Canvas" ).GetComponentInChildren<ModeChange>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
