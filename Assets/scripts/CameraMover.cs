﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour
{

    public float Speed = 10;
    private Vector3 m_camStart;
    private Vector3 m_moveLast;
    private Vector3 m_moveStart;
    private bool m_update = false;

    public Vector3 DeltaMove
    {
        get
        {
            return TargetPos - m_moveStart;
        }
    }
    public Vector3 TargetPos
    {
        get
        {
            if ( m_update ){
                m_moveLast = Camera.main.ScreenToWorldPoint( Input.mousePosition );                
            }
            return m_moveLast;
        }
    }

    // Use this for initialization
    void Start()
    {
        m_moveLast = transform.position;
        m_moveStart = transform.position;
        m_camStart = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = Vector2.zero;
        if ( Input.GetKey( KeyCode.W ) )
        {
            move += Vector2.up;
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            move += Vector2.down;
        }
        if ( Input.GetKey( KeyCode.A ) )
        {
            move += Vector2.left;
        }
        if ( Input.GetKey( KeyCode.D ) )
        {
            move += Vector2.right;
        }
        transform.Translate( move * Speed * Time.deltaTime );

        float zoom = Input.GetAxis("Mouse ScrollWheel");
        float size = Camera.main.orthographicSize;
        size = size - zoom * 25;
        Camera.main.orthographicSize = size;

        if ( Input.GetKeyDown( KeyCode.Mouse2 ) )
        {
            m_update = true;            
            m_moveStart = Camera.main.ScreenToWorldPoint( Input.mousePosition );
            m_camStart = transform.position;
        }
        if ( Input.GetKeyUp( KeyCode.Mouse2 ) )
        {
            m_update = false;
        }
        transform.Translate( (MovePosition - transform.position ) * Time.deltaTime);
    }

    public Vector3 MovePosition
    {
        get
        {
            return ( DeltaMove * -1f + m_camStart );
        }
    }
}
