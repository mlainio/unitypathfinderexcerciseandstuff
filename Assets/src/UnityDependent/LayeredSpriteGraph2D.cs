﻿using UnityEngine;
using System.Collections;
using System;

namespace Graph
{
    /// <summary>
    /// A version of nodegraph2D which uses layered sprite nodes, i.e. intended for visualization
    /// </summary>
    public class LayeredSpriteGraph2D : NodeGraph2D
    {
        private GameObject m_parent;
        public GameObject Parent
        {
            get
            {
                return m_parent;
            }
        }
        private Sprite m_gridSprite;
        public Sprite BaseGridSprite
        {
            get
            {
                try
                {
                    if ( m_gridSprite == null )
                        m_gridSprite = Resources.Load<Sprite>( "sprite_grid" );
                }
                catch ( System.Exception e )
                {
                    Debug.LogException( e );
                }

                return m_gridSprite;

            }
        }
        private Sprite m_nodeSprite;
        public Sprite BaseNodeSprite
        {
            get
            {
                try
                {
                    if ( m_nodeSprite == null )
                        m_nodeSprite = Resources.Load<Sprite>( "sprite_node" );
                }
                catch ( System.Exception e )
                {
                    Debug.LogException( e );
                }
                return m_nodeSprite;
            }
        }

        public LayeredSpriteGraph2D(GameObject gobj, int height, int width) : base(height, width)
        {
            m_parent = gobj;
        }

        public override Node2D CreateNode( int x, int y )
        {
            GameObject nodeObject = new GameObject( "GridNode " + x + " " + y );
            nodeObject.transform.position = new Vector3( x, y, 0 );
            nodeObject.transform.parent = Parent.transform;
            nodeObject.AddComponent<SpriteRenderer>().sprite = BaseGridSprite;

            LayeredSpriteNode2D node = new LayeredSpriteNode2D(x, y, nodeObject );

            return node;
        }

        public void ClearOverlay( ObjectLayer.Layer overlay )
        {
            foreach (Node2D node in m_nodes)
            {
                ( node as LayeredSpriteNode2D ).RemoveOverlaySprite( overlay );
            }
        }
        public void ClearEverything()
        {
            foreach ( Node2D node in m_nodes )
            {
                foreach ( ObjectLayer.Layer layer in Enum.GetValues( typeof( ObjectLayer.Layer ) ) )
                {
                    ( node as LayeredSpriteNode2D ).RemoveOverlaySprite(layer);
                }
            }

        }

        public override double NodeCostMultiplier( Node2D node )
        {
            try
            {
                return ( node as LayeredSpriteNode2D ).Cost;
            }
            catch (System.Exception e)
            {
                Debug.LogException( e );
            }
            return 1;
        }

        internal void deleteAllNodeObjects()
        {
            foreach (Node2D node in m_nodes)
            {
                //Debug.Log( "Looking to destroy node at " + node.Position );
                ( node as LayeredSpriteNode2D ).Destroy();
            }
        }
    }
}