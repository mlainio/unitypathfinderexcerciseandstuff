﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Graph
{
    /// <summary>
    /// Unity Node Object that implements overlayed sprites for visual representation
    /// </summary>
    public class LayeredSpriteNode2D : OverlayNode2D
    {
        public const string DATA_NODECOST = "SpriteOverlayNode2D.Data.Cost";
        public const string DATA_GAMEOBJECT = "SpriteOverlayNode2D.Data.GameObject";
        public GameObject BaseObject
        {
            get
            {
                return (GameObject) GetData( DATA_GAMEOBJECT );
            }

            set
            {
                SetData( DATA_GAMEOBJECT, value );
            }
        }

        public Vector2 PositionVector2
        {
            get
            {
                return new Vector2( Position.x, Position.y );
            }
        }
        public double Cost
        {
            get
            {
                return (double) GetData( DATA_NODECOST );
            }
            set
            {
                SetData( DATA_NODECOST, value );
            }
        }

        public LayeredSpriteNode2D( int x, int y, GameObject nodeObject ) : base( x, y )
        {
            BaseObject = nodeObject;
            Cost = 0;
        }

        /// <summary>
        /// Set a sprite to be drawn on the specified overlay.
        /// </summary>
        /// <param name="sprite"></param>
        /// <param name="color"></param>
        /// <param name="overlay"></param>
        public void SetOverlaySprite( Sprite sprite, Color32 color, ObjectLayer.Layer overlay )
        {
            //Debug.Log( "SetOverlaySprite" );
            // Lazy Loading
            if ( m_overlayObjects[ overlay ] == null )
            {
                m_overlayObjects[ overlay ] = GameObject.Instantiate( BaseObject );
            }
            else if ( !( m_overlayObjects[ overlay ] as GameObject ).activeSelf )
            {
                ( m_overlayObjects[ overlay ] as GameObject ).SetActive( true );
            }
            GameObject go = (m_overlayObjects[ overlay ] as GameObject);
            go.name = BaseObject.name + ".Overlay." + overlay;
            go.transform.parent = BaseObject.transform;
            SpriteRenderer rend = go.GetComponent<SpriteRenderer>();
            rend.sprite = sprite;
            rend.sortingLayerName = "GridOverlay";
            rend.sortingOrder = (int) overlay;
            rend.color = color;
        }

        public void RemoveOverlaySprite( ObjectLayer.Layer overlay )
        {

            //Debug.Log( "Removing overlaysprite in " + BaseObject.name + " from overlay " + overlay );
            if ( m_overlayObjects[ overlay ] != null )
            {
                if ( ( m_overlayObjects[ overlay ] as GameObject ).activeSelf )
                    ( m_overlayObjects[ overlay ] as GameObject ).SetActive( false );
                //Debug.Log( "Removed" );
            }
            // Destroying is a waste of resources. We can just disable the object until we need it again
            //GameObject.DestroyImmediate( m_overlayObjects[ overlay ] );

        }

        public void Destroy()
        {
            int destroyed = 1;
            foreach (ObjectLayer.Layer layer in Enum.GetValues(typeof(ObjectLayer.Layer)))
            {
                GameObject go = m_overlayObjects[layer] as GameObject;
                
                if ( go != null )
                {
                    destroyed++;
                    GameObject.Destroy( go );
                }
            }
            GameObject.Destroy( BaseObject );
            //Debug.Log( "Destroyed "+destroyed );
        }
    }

}