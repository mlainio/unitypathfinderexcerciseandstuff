﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graph
{
    /// <summary>
    /// A 2-dimensional Graphh using Node2D for the nodes
    /// Implement the virtual method CreateNode to use other types of nodes
    /// </summary>
    public class NodeGraph2D
    {
        public static readonly Point2 DIRECTION_UP = new Point2(0,1);
        public static readonly Point2 DIRECTION_DOWN = new Point2(0,-1);
        public static readonly Point2 DIRECTION_LEFT = new Point2(-1,0);
        public static readonly Point2 DIRECTION_RIGHT = new Point2(1,0);
        public static readonly Point2[] Directions = {DIRECTION_RIGHT, DIRECTION_LEFT, DIRECTION_UP, DIRECTION_DOWN };
        protected List<Node2D> m_nodes;
        private int m_width;
        private int m_height;

        public NodeGraph2D()
        {
            m_nodes = new List<Node2D>();
            m_width = 10;
            m_height = 10;
        }

        public NodeGraph2D(int height, int width)
        {
            m_nodes = new List<Node2D>();
            m_width = width;
            m_height = height;
        }

        private Node2D add( Node2D node )
        {
            m_nodes.Add( node );
            return node;
        }

        public void PopulateIterative()
        {
            for ( int x = 0; x < m_width; x++ )
            {
                for ( int y = 0; y < m_height; y++ )
                {
                    add( CreateNode( x, y ) );
                }
            }
        }

        public virtual Node2D CreateNode(int x, int y)
        {
            return add( new Node2D( x, y ) );
        }

        /// <summary>
        /// Populates the graph with width x height nodes
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void PopulateRecursive( Point2 startPosition, int width, int height )
        {
            //nope, not as simple.
            //foreach (Point p in GetNeighbourPoints(startPosition, width, height ) )
            //{                
            //    PopulateRecursive( p, width, height );
            //}
        }

        private int m_flowingIndex=0;
        /// <summary>
        /// Gets an array of points eligible for neighbours of the designated point in the graph        
        /// You still need to check wether the node is node an obstacle or not traversible
        /// </summary>
        /// <param name="point">The point whose neighbours we eant</param>
        /// <returns>an array of neighbour points within the graph boundaries</returns>
        public Point2[] GetNeighbourPoints( Point2 point)
        {
            List<Point2> result = new List<Point2>();
            for ( int i = 0; i < Directions.Length; i++ )
            {
                Point2 p = Directions[(m_flowingIndex+i)%Directions.Length];
                int x = point.x + p.x;
                int y = point.y + p.y;
                Point2 n = new Point2( x, y );
                if ( withinBounds( n ) )
                    result.Add( n );
            }
            m_flowingIndex++;
            return result.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Node2D GetNode( Point2 p )
        {
            if (!withinBounds(p))
                return null;

            int index = p.x * m_height + p.y;
            return m_nodes[ index ];
        }

        /// <summary>
        /// Check if point p falls within the boundaries of the graph considering its width and height
        /// </summary>
        /// <param name="p"></param>
        /// <returns>true if within bounds</returns>
        public bool withinBounds( Point2 p )
        {
            return ( p.x >= 0 && p.x < m_width && p.y >= 0 && p.y < m_height );
        }

        /// <summary>
        /// returns the mathematical cost to traverse from point a to point b
        ///  
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public double Cost( Point2 a, Point2 b)
        {
            return Distance( a, b )*NodeCostMultiplier( GetNode( b ) );
        }

        public virtual double NodeCostMultiplier( Node2D node )
        {
            return 1;
        }

        /// <summary>
        /// the distance between point a and point b
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public double Distance(Point2 a, Point2 b)
        {
            Point2 dif = b-a;
            int magnitude = (int)Math.Sqrt(dif.x*dif.x+dif.y*dif.y);
            return magnitude;
        }
    }
}
