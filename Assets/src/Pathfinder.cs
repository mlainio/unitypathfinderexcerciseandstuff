﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using UnityEngine;
using Graph;
using Priority_Queue;
namespace PathFinding
{
    public enum SearchAlgorithm
    {
        BreadthFirst,
        Djikstra,
        GreedyBestFirst,
        AStar
    }

    public class Pathfinder
    {       
        public interface ISearchCallback
        {
            void ClearFrontier( Node2D[] frontier );
            void UpdateFrontier( Node2D[] frontier );
            void UpdateVisited( Node2D[] visited );
            void Done( Node2D[] result );
        }
        private SearchAlgorithm m_algorithm = SearchAlgorithm.BreadthFirst;
        public SearchAlgorithm SearchAlgorithm
        {
            get
            {
                return m_algorithm;
            }
            set
            {
                m_algorithm = value;
            }
        }


        protected Graph2DSearch GraphSearcher
        {
            get
            {
                Graph2DSearch a = null;
                switch (m_algorithm)
                {
                    case SearchAlgorithm.AStar:
                        break;
                    case SearchAlgorithm.BreadthFirst:
                        a = m_bfSearch;
                        break;
                    case SearchAlgorithm.Djikstra:
                        a = m_dSearch;
                        break;
                    case SearchAlgorithm.GreedyBestFirst:
                        break;
                }
                return a;
            }
        }
        private LayeredSpriteGraph2D m_graph;
        private Graph2DSearch m_bfSearch;
        private DjiktsraSearch m_dSearch;        
        private ISearchCallback m_callback;
        private bool m_ongoing = false;

        public Pathfinder( LayeredSpriteGraph2D graph, ISearchCallback callback )
        {
            m_graph = graph;
            m_bfSearch = new BreadthFirstSearch( m_graph );
            m_dSearch = new DjiktsraSearch( m_graph );
            m_callback = callback;
        }

        public void Search( Node2D origin, Node2D goal )
        {
            //Debug.Log( "Search Start" );
            if ( !m_ongoing )
            {
                GraphSearcher.Init( origin );
                m_ongoing = true;
            }
            else
            {
                if ( !GraphSearcher.SearchOnce(goal) )
                {
                    if ( GraphSearcher.CurrentNode.Equals( goal ) )
                    {   //found the goal
                        m_callback.Done( GraphSearcher.GetPathTo( GraphSearcher.CurrentNode ) );
                    }
                    else
                    {   //failed to find anything
                        m_callback.Done( null );
                    }
                    m_ongoing = false;
                    m_callback.ClearFrontier( GraphSearcher.Frontier );
                    GraphSearcher.Reset();
                }
                else
                {
                    m_callback.UpdateFrontier( GraphSearcher.Frontier );
                }
            }
            
        }
    }

    public abstract class Graph2DSearch
    {
        /// <summary>
        /// Max iteration count for determining the path. Shouldn't need this at all, but it's for debugging purposes...
        /// </summary>
        private int m_timeout = 1000;
        protected NodeGraph2D m_graph;
        //protected Queue<OverlayNode2D> m_frontier = new Queue<OverlayNode2D>();
        protected SortedDictionary<Point2, Node2D> m_cameFrom = new SortedDictionary<Point2, Node2D>();
        protected Node2D m_origin;
        private OverlayNode2D m_currentNode;
        public OverlayNode2D CurrentNode
        {
            get
            {
                return m_currentNode;
            }
            set
            {
                m_currentNode = value;
            }
        }

        public abstract Node2D[] Frontier { get; }

        public virtual Node2D[] GetPathTo( OverlayNode2D goal )
        {
            int timeout = m_timeout;
            if ( m_origin == null )
            {
                Debug.Log( "Origin is null, cannot create path" );
                return null;
            }
            List<Node2D> path = new List<Node2D>();
            if ( goal.IsTraversible )
            {
                Node2D current = goal;

                path.Add( current );
                while ( current != m_origin )
                {
                    timeout--;
                    current = m_cameFrom[ current.Position ];
                    path.Add( current );

                    if ( timeout <= 0 )
                    {
                        Debug.Log( "Path Timed out, something went wrong..." );
                        break;
                    }
                }
            }


            path.Reverse();
            string p = "Path: ";
            foreach ( Node2D n in path )
                p += n.Position + "->";
            Debug.Log( p );
            return path.ToArray();
        }

        /// <summary>
        /// Visit the next node, removing it from the frontier and marking it visited
        /// </summary>
        /// <returns>visited node, can be null</returns>
        public OverlayNode2D VisitNext()
        {
            if ( HasFrontier )
            {
                OverlayNode2D nextNode = NextInFrontier as OverlayNode2D;
                //Debug.Log( "Visiting Node " + nextNode.Position );
                //if ( nextNode != null && !m_cameFrom.ContainsKey(nextNode.Position)) 
                //    m_cameFrom.Add( nextNode.Position, CurrentNode );

                return nextNode;
            }
            return null;
        }

        public abstract bool HasFrontier { get; }

        public OverlayNode2D[] Visited
        {
            get
            {
                OverlayNode2D[] p = new OverlayNode2D[m_cameFrom.Count];
                m_cameFrom.Values.CopyTo( p, 0 );
                return p;
            }
        }

        public abstract Node2D NextInFrontier { get; }

        public Graph2DSearch( NodeGraph2D graph )
        {
            m_graph = graph;
        }

        public void SetOrigin( OverlayNode2D node )
        {
            if ( node.IsTraversible )
            {
                m_origin = node;
            }
        }

        /// <summary>
        /// Do a 1 round search iteration.
        /// </summary>
        /// <returns>true if search is still valid, false if not and search should be stopped</returns>
        public abstract bool SearchOnce(Node2D goal);

        /// <summary>
        /// Initialize the search algorithm
        /// </summary>
        /// <param name="origin"></param>
        public abstract void Init( Node2D origin );

        public virtual void Reset()
        {
            m_cameFrom.Clear();
            m_currentNode = null;
            m_origin = null;
        }
    }

    public class BreadthFirstSearch : Graph2DSearch
    {
        protected Queue<OverlayNode2D> m_frontier = new Queue<OverlayNode2D>();
        protected int iteration = 0;
        public BreadthFirstSearch( NodeGraph2D graph ) : base( graph )
        {

        }

        public override Node2D[] Frontier
        {
            get
            {
                {
                    return m_frontier.ToArray();
                }
            }
        }

        public override bool HasFrontier
        {
            get
            {
                return m_frontier.Count > 0;
            }
        }

        public override Node2D NextInFrontier
        {
            get
            {
                if ( HasFrontier )
                    return m_frontier.Dequeue();
                return null;
            }
        }

        public override void Init( Node2D origin )
        {
            Debug.Log( "Initializing Breadth-First-Search" );
            SetOrigin( origin as OverlayNode2D );
            m_frontier.Enqueue( origin as OverlayNode2D );
            m_cameFrom.Add( m_origin.Position, Node2D.NullNode );
        }

        public override void Reset()
        {
            base.Reset();
            m_frontier.Clear();
        }

        /// <summary>
        /// One loop of the whole search. Repeat this as you see fit.
        /// </summary>
        public override bool SearchOnce(Node2D goal)
        {
            //Debug.Log( "Breadth-First-Search iterating " + iteration++ );
            CurrentNode = VisitNext();
            //found the goal
            if ( CurrentNode.Equals( goal ) )
                return false;
                        
            if ( CurrentNode != null )
            {
                foreach ( Point2 nextPoint in m_graph.GetNeighbourPoints( CurrentNode.Position ) )
                {
                    OverlayNode2D node = m_graph.GetNode(nextPoint) as OverlayNode2D;
                    // Node will be dismissed if it's not traversible: Not in the cameFrom list means not considered for path either!
                    if ( node.IsTraversible && !m_cameFrom.ContainsKey( nextPoint ) )
                    {
                        m_frontier.Enqueue( node as OverlayNode2D );
                        m_cameFrom.Add( nextPoint, CurrentNode );
                    }
                }
                return true;
            }
            return false;
            //failed to find anything and nothing to search
        }
    }

    public class DjiktsraSearch : Graph2DSearch
    {
        //protected PriorityQueue<PriorityNode2D> m_frontier = new PriorityQueue<PriorityNode2D>();
        protected SimplePriorityQueue<Node2D> m_frontier = new SimplePriorityQueue<Node2D>();
        private List<Node2D> m_frontierCopy = new List<Node2D>();
        protected Dictionary<Node2D, double> m_costSoFar = new Dictionary<Node2D, double>();
        public DjiktsraSearch( NodeGraph2D graph ) : base( graph ) { }

        // returns a list of all nodes in the frontier in some weird order.
        public override Node2D[] Frontier
        {
            get
            {
                return m_frontierCopy.ToArray();
            }
        }

        private void addToFrontier(Node2D node, double priority)
        {
            m_frontier.Enqueue( node, priority );
            m_frontierCopy.Add( node );
        }

        public override bool HasFrontier
        {
            get
            {
                return m_frontier.Count > 0;
            }
        }

        public override Node2D NextInFrontier
        {
            get
            {
                Node2D node = m_frontier.Dequeue();
                m_frontierCopy.Remove( node );
                return node;

            }
        }

        public override void Init( Node2D origin )
        {
            Debug.Log( "Djiktra's Search Algorithm" );
            SetOrigin( origin as OverlayNode2D );
            addToFrontier( origin, 0 );
            m_costSoFar.Add( origin, 0 );
            m_cameFrom.Add( origin.Position, Node2D.NullNode );
        }

        public override void Reset()
        {
            m_frontier.Clear();
            m_frontierCopy.Clear();
            m_costSoFar.Clear();
            m_cameFrom.Clear();
        }

        public override bool SearchOnce(Node2D goal)
        {
            CurrentNode = VisitNext();
            if ( CurrentNode.Equals( goal ) )
                return false;
            if ( CurrentNode != null )
            {
                foreach ( Point2 nextPoint in m_graph.GetNeighbourPoints( CurrentNode.Position ) )
                {
                    OverlayNode2D node = m_graph.GetNode(nextPoint) as OverlayNode2D;
                    double cost = m_costSoFar[CurrentNode] + m_graph.Cost(CurrentNode.Position, nextPoint);
                    if ( node.IsTraversible && ( !m_costSoFar.ContainsKey( node ) || cost < m_costSoFar[ node ] ))
                    {                        
                        // Node will be dismissed if it's not traversible: Not in the cameFrom list means not considered for path either!
                        m_costSoFar[ node ] = cost;
                        addToFrontier( node, cost );
                        //m_frontier.Enqueue( node, cost );
                        m_cameFrom.Add( nextPoint, CurrentNode );
                    }
                }
                return true;
            }
            return false;
        }
    }

}



