﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Util
{
    /// <summary>
    /// Generic DataObject with a key value type data storage
    /// </summary>
    public class DataObject
    {
        private Dictionary<string, object> m_nodeData;
        public DataObject()
        {
            m_nodeData = new Dictionary<string, object>();
        }
        public void SetData( string key, object value )
        {
            if ( m_nodeData.ContainsKey( key ) )
            {
                m_nodeData[ key ] = value;
            }
            else
            {
                m_nodeData.Add( key, value );
            }
        }
        public object GetData( string key )
        {
            if ( m_nodeData.ContainsKey( key ) )
            {
                return m_nodeData[ key ];
            }
            else return null;
        }
    }
}
