﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;

namespace Graph
{

    /// <summary>
    /// A Class representing a point in 2-dimensional space
    /// </summary>
    public struct Point2 : IComparable<Point2>
    {
        public int x;
        public int y;

        public Point2( int x, int y )
        {
            this.x = x;
            this.y = y;
        }

        public static Point2 operator +( Point2 left, Point2 right )
        {
            return new Point2( left.x + right.x, left.y + right.y );
        }
        public static Point2 operator -( Point2 left, Point2 right )
        {
            return new Point2( left.x - right.x, left.y - right.y );
        }
        public static bool operator ==(Point2 left, Point2 right)
        {
            return ( left.x == right.x && left.y == right.y );
        }
        public static bool operator !=( Point2 left, Point2 right )
        {
            return ( left.x != right.x || left.y != right.y );
        }

        public override string ToString()
        {
            return ( "(" + x + ", " + y + ")" );
        }
        public override bool Equals( object obj )
        {
            return this == ( ( Point2) obj );
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int CompareTo( Point2 other )
        {
            if ( this.x > other.x )
                return 1;
            if ( this.x < other.x )
                return -1;
            if ( this.x == other.x )
                if ( this.y < other.y )
                    return -1;
                else if ( this.y > other.y )
                    return 1;

            return 0;
        }
    }

    /// <summary>
    /// Basic 2-dimensional node with a position data property and 4 directions
    /// </summary>
    public class Node2D : DataObject
    {
        
        public const string DATA_POSITION = "Node2D.Data.Position";
        public static readonly Node2D NullNode = new Node2D( int.MinValue, int.MaxValue );
        public Point2 Position
        {
            get
            {
                return ((Point2)GetData( DATA_POSITION ));
            }
            set
            {
                SetData( DATA_POSITION, value );
            }
        }

        public Node2D(int x, int y)
        {
            Position = new Point2( x, y );
        }
    }

    /// <summary>
    /// A class to hold Objects on predefined layers which can be accessed with the enum via indexed default accessor
    /// </summary>
    public class ObjectLayer
    {
        public enum Layer
        {
            Floor,
            PassableObstacle,
            Wall,
            Goal,
            Origin,
            Frontier,
            Path,
        }
        protected object[] m_objects = new object[TypeCount];

        public static int TypeCount
        {
            get { return System.Enum.GetNames( typeof( Layer ) ).Length; }
        }

        public object this[ Layer type ]
        {
            get
            {
                return m_objects[ (int) type ];
            }
            set
            {
                m_objects[ (int) type ] = value;
            }
        }
        public System.Collections.IEnumerator GetEnumerator()
        {
            foreach ( Layer l in Enum.GetValues( typeof( Layer ) ) )
                yield return m_objects[(int) l];
        }
    }    

    /// <summary>
    /// A node which can be prioritized based on a "cost", made to be used with PriorityQueue
    /// </summary>
    /// USELESS CLASS
    /// 
    [Obsolete]
    public class PriorityNode2D : Node2D, System.IComparable
    {
        protected int m_cost = 0;
        public int Cost
        {
            get { return m_cost; }
            set { m_cost = value; }
        }
        public PriorityNode2D( int x, int y ) : base( x, y ) { }

        public int CompareTo( object obj )
        {
            int value = 0;
            try
            {
                value = ( obj as PriorityNode2D ).Cost - Cost;
            } catch (Exception e)
            {
            }
            return value;
        }
    }

    /// <summary>
    /// A node which contains an ObjectOverlay overlay
    /// </summary>
    public class OverlayNode2D : Node2D
    {
        protected ObjectLayer m_overlayObjects;
        public OverlayNode2D( int x, int y ) : base( x, y )
        {
            m_overlayObjects = new ObjectLayer();
        }

        public object GetOverlay( ObjectLayer.Layer overlay )
        {
            return m_overlayObjects[ overlay ];
        }

        public void SetOverlay( ObjectLayer.Layer overlay, object overlayObject )
        {
            m_overlayObjects[ overlay ] = overlayObject;
        }

        public bool IsObstacle
        {
            get
            {
                return GetOverlay( ObjectLayer.Layer.Wall) != null;
            }
        }
        public bool IsTraversible
        {
            get
            {
                return GetOverlay( ObjectLayer.Layer.Floor ) != null && !IsObstacle;
            }
        }
    }

}
